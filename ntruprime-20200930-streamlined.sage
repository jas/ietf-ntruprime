sntrup = { # p,q,w
  'sntrup761':    (761,4591,286),
  'sntrup653':    (653,4621,288),
  'sntrup857':    (857,5167,322),
  'sntrup953':    (953,6343,396),
  'sntrup1013':   (1013,7177,448),
  'sntrup1277':   (1277,7879,492),
}

def setparameters(system,random8):
  global p
  global q
  global w

  global PublicKeys_bytes
  global SecretKeys_bytes
  global Ciphertexts_bytes
  global Inputs_bytes
  global Confirm_bytes
  global Hash_bytes
  global Rq_encode
  global Inputs_random
  global ZKeyGen
  global ZEncrypt
  global ZDecrypt

  if system in sntrup:
    p,q,w = sntrup[system]
  else:
    raise Exception('%s is not one of the selected parameter sets' % system)

  # ----- parameter requirements
  # tested later: irreducibility of x^p-x-1 mod q

  assert p.is_prime()
  assert q.is_prime()
  assert w > 0
  assert 2*p >= 3*w
  assert q >= 16*w+1
  assert q%6 == 1 # spec allows 5 but these tests do not
  assert p%4 == 1 # spec allows 3 but ref C code does not

  # ----- arithmetic mod 3

  F3 = GF(3)
  def ZZ_fromF3(c):
    assert c in F3
    return ZZ(c+1)-1

  # ----- arithmetic mod q

  Fq = GF(q)
  q12 = ZZ((q-1)/2)
  def ZZ_fromFq(c):
    assert c in Fq
    return ZZ(c+q12)-q12

  # ----- polynomials over integers

  global R

  Zx.<x> = ZZ[]
  R.<xp> = Zx.quotient(x^p-x-1)

  def Weightw_is(r):
    assert r in R
    return w == len([i for i in range(p) if r[i] != 0])

  def Small_is(r):
    assert r in R
    return all(abs(r[i]) <= 1 for i in range(p))

  def Short_is(r):
    return Small_is(r) and Weightw_is(r)

  # ----- polynomials mod 3

  F3x.<x3> = F3[]
  R3.<x3p> = F3x.quotient(x^p-x-1)

  def R_fromR3(r):
    assert r in R3
    return R([ZZ_fromF3(r[i]) for i in range(p)])

  def R3_fromR(r):
    assert r in R
    return R3([r[i] for i in range(p)])

  # ----- polynomials mod q

  Fqx.<xq> = Fq[]
  assert (xq^p-xq-1).is_irreducible()

  global Rq
  Rq.<xqp> = Fqx.quotient(x^p-x-1)

  global R_fromRq
  def R_fromRq(r):
    assert r in Rq
    return R([ZZ_fromFq(r[i]) for i in range(p)])

  global Rq_fromR
  def Rq_fromR(r):
    assert r in R
    return Rq([r[i] for i in range(p)])

  # ----- rounded polynomials mod q

  def Rounded_is(r):
    assert r in R
    return (all(r[i]%3 == 0 for i in range(p))
      and all(r[i] >= -q12 for i in range(p))
      and all(r[i] <= q12 for i in range(p)))

  def Round(a):
    assert a in Rq
    c = R_fromRq(a)
    r = [3*round(c[i]/3) for i in range(p)]
    assert all(abs(r[i]-c[i]) <= 1 for i in range(p))
    r = R(r)
    assert Rounded_is(r)
    return r

  # ----- sorting to generate short polynomial

  global Short_fromlist
  def Short_fromlist(L): # L is list of p uint32
    L = [L[i]&-2 for i in range(w)] + [(L[i]&-3)|1 for i in range(w,p)]
    assert all(L[i]%2 == 0 for i in range(w))
    assert all(L[i]%4 == 1 for i in range(w,p))
    L.sort()
    L = [(L[i]%4)-1 for i in range(p)]
    assert all(abs(L[i]) <= 1 for i in range(p))
    assert sum(abs(L[i]) for i in range(p)) == w
    r = R(L)
    assert Short_is(r)
    return r

  # ----- underlying hash function

  import hashlib

  global sha512
  def sha512(s):
    h = hashlib.sha512()
    h.update(s)
    return h.digest()

  Hash_bytes = 32
  def Hash(s): return sha512(s)[:Hash_bytes]

  def Hash0(s): return Hash(chr(0)+s)
  def Hash1(s): return Hash(chr(1)+s)
  def Hash2(s): return Hash(chr(2)+s)
  def Hash3(s): return Hash(chr(3)+s)
  def Hash4(s): return Hash(chr(4)+s)

  # ----- higher-level randomness

  def urandom32():
    c0 = random8()
    c1 = random8()
    c2 = random8()
    c3 = random8()
    return c0 + 256*c1 + 65536*c2 + 16777216*c3

  global Short_random
  def Short_random(): # R element with w coeffs +-1
    L = [urandom32() for i in range(p)]
    return Short_fromlist(L)

  def randomrange3():
    return ((urandom32() & 0x3fffffff) * 3) >> 30

  def Small_random():
    r = R([randomrange3()-1 for i in range(p)])
    assert Small_is(r)
    return r

  # ----- Streamlined NTRU Prime Core

  global KeyGen
  global Encrypt
  global Decrypt

  def KeyGen():
    while True:
      g = Small_random()
      if R3_fromR(g).is_unit(): break
    f = Short_random()
    h = Rq_fromR(g)/Rq_fromR(3*f)
    return h,(f,1/R3_fromR(g))

  def Encrypt(r,h):
    assert Short_is(r)
    assert h in Rq
    return Round(h*Rq_fromR(r))

  def Decrypt(c,k):
    f,v = k
    assert Rounded_is(c)
    assert Short_is(f)
    assert v in R3
    e = R3_fromR(R_fromRq(3*Rq_fromR(f)*Rq_fromR(c)))
    r = R_fromR3(e*v)
    if Weightw_is(r): return r
    return R([1]*w+[0]*(p-w))

  # ----- strings

  global tostring
  def tostring(s):
    return ''.join(chr(si) for si in s)

  def fromstring(s):
    return [ord(si) for si in s]

  # ----- encoding small polynomials (including short polynomials)

  Small_bytes = ceil(p/4)

  def Small_encode(r):
    assert Small_is(r)
    R = [r[i]+1 for i in range(p)]
    while len(R) < 4*Small_bytes: R += [0]
    assert all(R[i] >= 0 for i in range(4*Small_bytes))
    assert all(R[i] <= 2 for i in range(4*Small_bytes))
    assert len(R) >= p
    assert len(R)%4 == 0
    S = [R[i]+4*R[i+1]+16*R[i+2]+64*R[i+3] for i in range(0,len(R),4)]
    return tostring(S)

  def Small_decode(s):
    S = fromstring(s)
    r = [(S[i//4]//4^(i%4))%4 for i in range(p)]
    assert all(r[i] >= 0 for i in range(p))
    assert all(r[i] <= 2 for i in range(p))
    r = [r[i]-1 for i in range(p)]
    return R(r)

  # ----- infrastructure for more general encoding

  limit = 16384

  def Encode(R,M):
    if len(M) == 0: return []
    S = []
    if len(M) == 1:
      r,m = R[0],M[0]
      while m > 1:
        S += [r%256]
        r,m = r//256,(m+255)//256
      return S
    R2,M2 = [],[]
    for i in range(0,len(M)-1,2):
      m,r = M[i]*M[i+1],R[i]+M[i]*R[i+1]
      while m >= limit:
        S += [r%256]
        r,m = r//256,(m+255)//256
      R2 += [r]
      M2 += [m]
    if len(M)&1:
      R2 += [R[-1]]
      M2 += [M[-1]]
    return S+Encode(R2,M2)

  def Decode(S,M):
    if len(M) == 0: return []
    if len(M) == 1: return [sum(S[i]*256**i for i in range(len(S)))%M[0]]
    k = 0
    bottom,M2 = [],[]
    for i in range(0,len(M)-1,2):
      m,r,t = M[i]*M[i+1],0,1
      while m >= limit:
        r,t,k,m = r+S[k]*t,t*256,k+1,(m+255)//256
      bottom += [(r,t)]
      M2 += [m]
    if len(M)&1:
      M2 += [M[-1]]
    R2 = Decode(S[k:],M2)
    R = []
    for i in range(0,len(M)-1,2):
      r,t = bottom[i//2]
      r += t*R2[i//2]
      R += [r%M[i]]
      R += [(r//M[i])%M[i+1]]
    if len(M)&1:
      R += [R2[-1]]
    return R

  # ----- encoding general polynomials

  def Rq_encode(r):
    assert r in Rq
    R = [ZZ_fromFq(r[i])+q12 for i in range(p)]
    M = [q]*p
    assert all(0 <= R[i] for i in range(p))
    assert all(R[i] < M[i] for i in range(p))
    return tostring(Encode(R,M))

  def Rq_decode(s):
    assert len(s) == Rq_bytes
    M = [q]*p
    R = Decode(fromstring(s),M)
    assert all(0 <= R[i] for i in range(p))
    assert all(R[i] < M[i] for i in range(p))
    r = [R[i]-q12 for i in range(p)]
    return Rq(r)

  global Rq_bytes
  Rq_bytes = len(Rq_encode(Rq(0)))

  # ----- encoding rounded polynomials

  def Rounded_encode(r):
    assert Rounded_is(r)
    R = [ZZ((ZZ_fromFq(r[i])+q12)/3) for i in range(p)]
    M = [ZZ((q-1)/3+1)]*p
    assert all(0 <= R[i] for i in range(p))
    assert all(R[i] < M[i] for i in range(p))
    return tostring(Encode(R,M))

  def Rounded_decode(s):
    assert len(s) == Rounded_bytes
    M = [ZZ((q-1)/3+1)]*p
    r = Decode(fromstring(s),M)
    assert all(0 <= r[i] for i in range(p))
    assert all(r[i] < M[i] for i in range(p))
    r = [3*r[i]-q12 for i in range(p)]
    return R(r)

  global Rounded_bytes
  Rounded_bytes = len(Rounded_encode(R(0)))

  # ----- Streamlined NTRU Prime Core plus encoding

  Inputs_random = Short_random
  Inputs_encode = Small_encode
  Inputs_bytes = Small_bytes

  Ciphertexts_bytes = Rounded_bytes
  SecretKeys_bytes = 2*Small_bytes
  PublicKeys_bytes = Rq_bytes

  def Inputs_randomenc():
    rho = [random8() for i in range(Small_bytes)]
    return tostring(rho)

  def ZKeyGen():
    h,(f,v) = KeyGen()
    return Rq_encode(h),Small_encode(f)+Small_encode(R_fromR3(v))

  def ZEncrypt(r,pk):
    assert len(pk) == PublicKeys_bytes
    h = Rq_decode(pk)
    return Rounded_encode(Encrypt(r,h))

  def ZDecrypt(c,sk):
    assert len(sk) == SecretKeys_bytes
    assert len(c) == Ciphertexts_bytes
    f = Small_decode(sk[:Small_bytes])
    v = R3_fromR(Small_decode(sk[Small_bytes:]))
    c = Rounded_decode(c)
    return Decrypt(c,(f,v))

  # ----- confirmation hash

  Confirm_bytes = 32

  def HashConfirm(r,K,cache=None):
    assert len(r) == Inputs_bytes
    assert len(K) == PublicKeys_bytes
    if not cache: cache = Hash4(K)
    assert cache == Hash4(K)
    r = Hash3(r)
    return Hash2(r+cache)

  # ----- session-key hash

  global HashSession
  def HashSession(b,y,z):
    assert len(y) == Inputs_bytes
    assert len(z) == Ciphertexts_bytes+Confirm_bytes
    assert b in [0,1]
    y = Hash3(y)
    if b == 1: return Hash1(y+z)
    return Hash0(y+z)

  # ----- Streamlined NTRU Prime

  # KeyGen' in Streamlined NTRU Prime spec
  global KEM_KeyGen
  def KEM_KeyGen():
    pk,sk = ZKeyGen()
    sk += pk
    rho = Inputs_randomenc()
    sk += rho
    sk += Hash4(pk)
    return pk,sk

  global Hide
  def Hide(r,pk,cache=None):
    r_enc = Inputs_encode(r)
    c = ZEncrypt(r,pk)
    gamma = HashConfirm(r_enc,pk,cache)
    c = c+gamma
    return c,r_enc

  global Encap
  def Encap(pk):
    r = Inputs_random()
    C,r_enc = Hide(r,pk)
    return C,HashSession(1,r_enc,C)

  global Decap
  def Decap(C,sk):
    Corig = C
    c_inner,C = C[:Ciphertexts_bytes],C[Ciphertexts_bytes:]
    gamma,C = C[:Confirm_bytes],C[Confirm_bytes:]
    assert len(C) == 0

    sk_inner,sk = sk[:SecretKeys_bytes],sk[SecretKeys_bytes:]
    pk,sk = sk[:PublicKeys_bytes],sk[PublicKeys_bytes:]
    rho,sk = sk[:Inputs_bytes],sk[Inputs_bytes:]
    cache = None
    cache,sk = sk[:Hash_bytes],sk[Hash_bytes:]
    assert len(sk) == 0

    r = ZDecrypt(c_inner,sk_inner)
    Cnew,r_enc = Hide(r,pk,cache)

    assert len(r_enc) == Inputs_bytes
    assert len(rho) == Inputs_bytes

    if Cnew == Corig: return HashSession(1,r_enc,Corig)
    return HashSession(0,rho,Corig)
