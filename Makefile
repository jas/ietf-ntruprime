DSSH=draft-ietf-sshm-ntruprime-ssh
DHYBRID=draft-josefsson-ntruprime-hybrid
DSTREAMLINED=draft-josefsson-ntruprime-streamlined

CDEPS=sntrup761-test.c sntrup761.c drbg-ctr.c sha512.c aes256_ecb.c
HDEPS=sntrup761.h drbg-ctr.h sha512.h aes256_ecb.h

all: expand/$(DSTREAMLINED).xml $(DSTREAMLINED).txt $(DSTREAMLINED).html $(DHYBRID).txt $(DHYBRID).html $(DSSH).txt $(DSSH).html sntrup761-test
	./sntrup761-test

clean:
	rm -f *~ expand/$(DSTREAMLINED).xml $(DSTREAMLINED).txt $(DSTREAMLINED).html $(DHYBRID).txt $(DHYBRID).html $(DSSH).txt $(DSSH).html sntrup761-test
	test ! -d expand || rmdir expand

sntrup761-test: sntrup761-test.c sntrup761.c drbg-ctr.c sha512.c aes256_ecb.c
	cc sntrup761-test.c sntrup761.c drbg-ctr.c sha512.c aes256_ecb.c -o sntrup761-test -lssl -lcrypto

expand/$(DSTREAMLINED).xml: $(DSTREAMLINED).xml $(CDEPS) $(HDEPS)
	mkdir -p expand
	xml2rfc --expand $< --out expand/$<

$(DSTREAMLINED).txt: $(DSTREAMLINED).xml $(CDEPS) $(HDEPS)
	xml2rfc $<

$(DSTREAMLINED).html: $(DSTREAMLINED).xml $(CDEPS) $(HDEPS)
	xml2rfc --html $<

$(DHYBRID).txt: $(DHYBRID).xml
	xml2rfc $<

$(DHYBRID).html: $(DHYBRID).xml
	xml2rfc --html $<

$(DSSH).txt: $(DSSH).xml
	xml2rfc $<

$(DSSH).html: $(DSSH).xml
	xml2rfc --html $<

draft-ietf-sshm-ntruprime-ssh-from-josefsson-ntruprime-ssh-03.diff.html:
	rfcdiff archive/draft-josefsson-ntruprime-ssh-03.txt draft-josefsson-ntruprime-ssh.txt
