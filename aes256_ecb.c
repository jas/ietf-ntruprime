#include <openssl/evp.h>

#include "aes256_ecb.h"

void
aes256_encrypt (const unsigned char *key,
		unsigned char *out,
		const unsigned char *in)
{
  EVP_CIPHER_CTX *ctx;
  int len;

  if(!(ctx = EVP_CIPHER_CTX_new()))
    abort();

  if(1 != EVP_EncryptInit_ex(ctx, EVP_aes_256_ecb(), NULL, key, NULL))
    abort();

  if(1 != EVP_EncryptUpdate(ctx, out, &len, in, AES_BLOCK_SIZE))
    abort();

  EVP_CIPHER_CTX_free(ctx);
}
