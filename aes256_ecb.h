#define AES_BLOCK_SIZE 16
#define AES256_KEY_SIZE 32

extern void
aes256_encrypt (const unsigned char *key,
		unsigned char *out,
		const unsigned char *in);
